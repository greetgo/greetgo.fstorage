package kz.greetgo.file_storage.impl.testing_grid_fs;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import kz.greetgo.file_storage.FileStorage;
import kz.greetgo.file_storage.impl.FileStorageBuilder;
import kz.greetgo.file_storage.impl.util.MongodbUtil;
import kz.greetgo.file_storage.impl.util.StrUtil;

public abstract class FsOperationAbstract {

  protected FileStorage fileStorage;

  protected void initFileStorage(int number) {

    String N = StrUtil.toLenZero(number, 3);

    //noinspection resource
    final MongoClient   mongoClient = MongodbUtil.createMongoClient();
    final MongoDatabase db          = mongoClient.getDatabase("test_collection_" + N);

    fileStorage = FileStorageBuilder.newBuilder()
                                    .inMongoGridFs(db)
                                    .chunkSizeBytes(255 * 1024)
                                    .build();
  }
}
