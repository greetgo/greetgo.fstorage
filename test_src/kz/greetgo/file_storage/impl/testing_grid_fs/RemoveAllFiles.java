package kz.greetgo.file_storage.impl.testing_grid_fs;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSFindIterable;
import com.mongodb.client.gridfs.model.GridFSFile;
import java.util.ArrayList;
import java.util.List;
import kz.greetgo.file_storage.FileStorage;
import kz.greetgo.file_storage.impl.FileStorageBuilder;
import kz.greetgo.file_storage.impl.util.MongodbUtil;
import org.bson.BsonDocument;
import org.bson.BsonValue;

public class RemoveAllFiles {
  public static void main(String[] args) {
    try (MongoClient mongoClient = MongodbUtil.createMongoClient()) {
      final MongoDatabase db = mongoClient.getDatabase("test_collection_001");


      List<BsonValue> smallFileIds=new ArrayList<>();

      final GridFSBucket bucket = GridFSBuckets.create(db);

      final GridFSFindIterable iterable = bucket.find(new BsonDocument());

      try (MongoCursor<GridFSFile> iterator = iterable.iterator()) {

        while (iterator.hasNext()) {
          final GridFSFile file   = iterator.next();
          final BsonValue  id     = file.getId();
          final long       length = file.getLength();

          //if (length < 1000) {
            smallFileIds.add(id);
          //}

          System.out.println("3wfG13S3aQ :: id = " + id + ", length = " + length);
        }

      }

      System.out.println("PO2l4Z4vb7 :: deleting");

      for (final BsonValue smallFileId : smallFileIds) {
        bucket.delete(smallFileId);
        System.out.println("dJnTQonIVB :: delete file with id = "+smallFileId);
      }

    }
  }
}
