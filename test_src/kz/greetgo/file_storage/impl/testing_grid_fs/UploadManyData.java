package kz.greetgo.file_storage.impl.testing_grid_fs;

import java.util.concurrent.atomic.AtomicBoolean;
import kz.greetgo.file_storage.impl.util.RND;

import static java.nio.charset.StandardCharsets.UTF_8;

public class UploadManyData extends FsOperationAbstract {

  public static void main(String[] args) throws Exception {
    final UploadManyData uploadManyData = new UploadManyData();
    uploadManyData.initFileStorage(1);
    uploadManyData.uploadManyData();
  }

  private void uploadManyData() throws InterruptedException {
    final AtomicBoolean working     = new AtomicBoolean(true);
    final AtomicBoolean printStatus = new AtomicBoolean(false);
    final Thread statusThread = new Thread(() -> {
      while (working.get()) {
        try {
          //noinspection BusyWait
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          return;
        }
        printStatus.set(true);
      }
    });
    statusThread.start();

    int insertCount = 0;

    for (int i = 0; i < 1; i++) {

      String content = RND.str(10 * 1024 * 1024);

      fileStorage.storing().data(content.getBytes(UTF_8))
                 .name("test file " + i + ".txt")
                 .store();
      insertCount++;

      if (printStatus.get()) {
        printStatus.set(false);
        System.out.println("zM9LuHRu7c :: Inserted " + insertCount);
      }
    }

    System.out.println("mWbROxxI8g :: Total inserted " + insertCount);

    working.set(false);
    statusThread.interrupt();
    statusThread.join();

  }
}
