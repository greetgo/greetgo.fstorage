package kz.greetgo.file_storage.impl.testing_grid_fs;

import kz.greetgo.file_storage.FileDataReader;

import static java.nio.charset.StandardCharsets.UTF_8;

public class FileStorageExperiments extends FsOperationAbstract {
  public static void main(String[] args) {
    final FileStorageExperiments x = new FileStorageExperiments();
    x.initFileStorage(1);
    x.experiments4();
  }

  private void experiments1() {

    final FileDataReader read = fileStorage.read("d76f0fbbe2f5b75fde60cb02");
    System.out.println("Y0yFsxXCqS :: " + read.name());

  }

  private void experiments2() {

    fileStorage.updating().param("l5QzUhUm1p_updating", "yes").store("d76f0fbbe2f5b75fde60cb02");

  }

  private void experiments3() {

    final FileDataReader read    = fileStorage.read("d76f0fbbe2f5b75fde60cb02");
    final String         par_val = read.paramValue("l5QzUhUm1p_updating");
    System.out.println("ZUBWnUMwWk :: par_val = " + par_val);

  }

  private void experiments4() {

    final String fileId = fileStorage.storing()
                                     .name("Hello.txt")
                                     .param("tst", "wow")
                                     .param("tst2", "wow2")
                                     .data("Hello World".getBytes(UTF_8))
                                     .store();

    System.out.println("ZUBWnUMwWk :: fileId = " + fileId);

  }

  private void experiments5() {
    final FileDataReader read = fileStorage.read("b5a21b4dbe1074c29c9e0fc4");

    System.out.println("7FqqVPp5Qp :: " + read.allParams());

  }
}
