package kz.greetgo.file_storage.impl.util;

import javax.annotation.Nonnull;

public class StrUtil {

  public static @Nonnull String toLenZero(int number, int length) {
    String s = "" + number;
    if (s.length() >= length) {
      return s;
    }
    StringBuilder sb = new StringBuilder(length);
    for (int i = 0, C = length - s.length(); i < C; i++) {
      sb.append('0');
    }
    sb.append(s);
    return sb.toString();
  }
}
