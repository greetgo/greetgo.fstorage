package kz.greetgo.file_storage.impl.util;

import com.mongodb.lang.NonNull;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import kz.greetgo.db.DbType;
import kz.greetgo.file_storage.impl.db.ConnectionManager;

public class TestUtil {
  public static @NonNull DataSource createFrom(DbType dbType, String schemaSuffix) {
    ConnectionManager connectionManager = ConnectionManager.get(dbType);
    connectionManager.setDbSchema(schemaSuffix);
    return new AbstractDataSource() {
      @Override
      public Connection getConnection() throws SQLException {
        try {
          return connectionManager.getNewConnection();
        } catch (SQLException e) {
          throw e;
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }
    };
  }
}
