package kz.greetgo.file_storage.impl.util;

import com.mongodb.MongoSocketOpenException;
import com.mongodb.MongoTimeoutException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.util.concurrent.atomic.AtomicReference;
import javax.annotation.Nonnull;
import org.bson.Document;

import static com.mongodb.client.model.Filters.eq;

public class MongodbUtil {

  public static @Nonnull MongoClient createMongoClient() {
    return MongoClients.create("mongodb://localhost:1702");
  }

  public @Nonnull
  static MongoDatabase createDatabase(String collectionName) {
    MongoClient mongoClient = createMongoClient();
    return mongoClient.getDatabase(System.getProperty("user.name") + "_" + collectionName);
  }

  public @Nonnull
  static MongoCollection<Document> connectGetCollection(String collectionName) {
    MongoDatabase database = createDatabase(collectionName);
    return database.getCollection("fileStorage");
  }

  private static final AtomicReference<Boolean> cachedHasMongodbResult = new AtomicReference<>(null);

  @SuppressWarnings("BooleanMethodIsAlwaysInverted")
  public static boolean hasMongodb() {
    {
      Boolean result = cachedHasMongodbResult.get();
      if (result != null) {
        return result;
      }
    }

    final AtomicReference<Boolean> detectedResult = new AtomicReference<>(null);

    Thread detector = new Thread(() -> {
      try {
        connectGetCollection("probe_collection").find(eq("asd", 1)).first();
        detectedResult.set(true);
      } catch (MongoSocketOpenException | MongoTimeoutException ignore) {
        detectedResult.set(false);
      }
    });

    detector.start();

    try {
      detector.join(1000);
    } catch (InterruptedException ignore) {
    }

    Boolean detectedRes = detectedResult.get();

    final boolean ret;

    if (detectedRes == null) {
      ret = false;
      detector.interrupt();
    } else {
      ret = detectedRes;
    }

    cachedHasMongodbResult.set(ret);

    return ret;
  }
}
