package kz.greetgo.file_storage.impl;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import kz.greetgo.file_storage.FileDataReader;
import kz.greetgo.file_storage.FileStorage;
import kz.greetgo.file_storage.impl.util.MongodbUtil;
import kz.greetgo.file_storage.impl.util.RND;
import org.fest.assertions.data.MapEntry;
import org.testng.annotations.Test;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.fest.assertions.api.Assertions.assertThat;

public class FileStorageBuilderInMongoGridFsImplTest {

  // TODO pompei написать здесь тесты на все аспекты

  @Test
  public void single_storeAndLoad() {
    try (MongoClient mongoClient = MongodbUtil.createMongoClient()) {
      final MongoDatabase db = mongoClient.getDatabase("test_collection_001");

      final FileStorage fileStorage = FileStorageBuilder.newBuilder()
                                                        .inMongoGridFs(db)
                                                        .chunkSizeBytes(255 * 1024)
                                                        .build();

      String content = "Это содержимое файла " + RND.str(10);

      final String fileId = fileStorage.storing().data(content.getBytes(UTF_8))
                                       .name("test file.txt")
                                       .store();

      final FileDataReader read = fileStorage.read(fileId);
      assertThat(read.name()).isEqualTo("test file.txt");
      assertThat(read.dataAsArray()).isEqualTo(content.getBytes(UTF_8));

    }
  }

  @Test
  public void update() {
    try (MongoClient mongoClient = MongodbUtil.createMongoClient()) {
      final MongoDatabase db = mongoClient.getDatabase("test_collection_001");

      final FileStorage fileStorage = FileStorageBuilder.newBuilder()
                                                        .inMongoGridFs(db)
                                                        .chunkSizeBytes(255 * 1024)
                                                        .build();

      String content = "Это содержимое файла " + RND.str(10);

      final String fileId = fileStorage.storing().data(content.getBytes(UTF_8))
                                       .name("test file.txt")
                                       .store();

      {
        final FileDataReader read = fileStorage.read(fileId);
        assertThat(read.name()).isEqualTo("test file.txt");
        assertThat(read.allParams()).isEmpty();
        assertThat(read.dataAsArray()).isEqualTo(content.getBytes(UTF_8));
      }

      //
      //
      fileStorage.updating().param("stone", "PkC11m3QmJ").param("hello", "world").name("new test name.txt").store(fileId);
      //
      //

      {
        final FileDataReader read = fileStorage.read(fileId);
        assertThat(read.name()).isEqualTo("new test name.txt");
        assertThat(read.allParams()).contains(MapEntry.entry("hello", "world"));
        assertThat(read.allParams()).contains(MapEntry.entry("stone", "PkC11m3QmJ"));
        assertThat(read.dataAsArray()).isEqualTo(content.getBytes(UTF_8));
      }

    }
  }
}