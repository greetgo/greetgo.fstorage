package kz.greetgo.file_storage.impl.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.testng.annotations.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class ConnectionManagerPostgreSQLTest {

  @Test
  public void checkConnectionToDb() {
    final ConnectionManagerPostgreSQL cm = new ConnectionManagerPostgreSQL();

    cm.setDbSchema("hello");

    try (Connection connection = cm.getNewConnection()) {

      try (PreparedStatement ps = connection.prepareStatement("select ? as asd")) {
        ps.setString(1, "qg1oMTnpXQ");
        try (ResultSet rs = ps.executeQuery()) {
          if (!rs.next()) {
            throw new RuntimeException("jBT4jS1B7I :: ERR");
          }
          final String string = rs.getString(1);
          assertThat(string).isEqualTo("qg1oMTnpXQ");
        }
      }

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}