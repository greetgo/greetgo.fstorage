package kz.greetgo.file_storage.impl.db;

import java.util.Arrays;

import static java.util.stream.Collectors.joining;

public class PgUrlBuilder {
  private final String host;
  private final int    port;
  private final String dbName;

  private String rock;

  public PgUrlBuilder(String host, int port, String dbName) {
    this.host   = host;
    this.port   = port;
    this.dbName = dbName;
  }

  /**
   * Фабричный метод данного объекта
   *
   * @param host   список хостов, где расположены сервера одного кластера PostgreSQL, разделённых через запятую.
   *               На всех этих серверах нужно расположить одинаковый порт
   * @param port   порт, на котором подняты сервера, хосты которых перечисленны в параметре host
   * @param dbName имя БД по этим хостам
   * @return сформированный объект-строитель
   */
  public static PgUrlBuilder on(String host, int port, String dbName) {
    return new PgUrlBuilder(host, port, dbName);
  }

  /**
   * Указывает предназначение доступа к БД - для чего этот доступ используется
   *
   * @param rock строка описывающая предназначение. Не стоит чтобы в ней был символ тире (-)
   * @return this
   */
  public PgUrlBuilder rock(String rock) {
    this.rock = rock;
    return this;
  }

  /**
   * Формирует URL доступа к БД PostgreSQL из ранее установленных данных
   *
   * @return URL доступа к БД PostgreSQL
   */
  public String build() {
    final StringBuilder applicationName = new StringBuilder();
    applicationName.append("fstorage");

    {
      final String rock = this.rock;
      if (rock != null) {
        applicationName.append("-").append(rock);
      }
    }

    String hostPort = Arrays.stream(host.split(","))
                            .map(String::trim)
                            .map(host -> host.indexOf(':') > 0 ? host : host + ":" + port)
                            .collect(joining(","));

    return "jdbc:postgresql://" + hostPort + "/" + (dbName == null ? "" : dbName) + "?ApplicationName=" + applicationName;
  }
}
