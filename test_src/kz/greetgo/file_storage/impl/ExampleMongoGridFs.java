package kz.greetgo.file_storage.impl;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import kz.greetgo.file_storage.FileStorage;
import kz.greetgo.file_storage.impl.util.MongodbUtil;
import kz.greetgo.file_storage.impl.util.RND;

import static java.nio.charset.StandardCharsets.UTF_8;

public class ExampleMongoGridFs {
  public static void main(String[] args) {

    try (MongoClient mongoClient = MongodbUtil.createMongoClient()) {
      final MongoDatabase db = mongoClient.getDatabase("test_collection_001");

      final FileStorage fileStorage = FileStorageBuilder.newBuilder()
                                                        .inMongoGridFs(db)
                                                        .chunkSizeBytes(255 * 1024)
                                                        .build();

      String content1 = RND.str(100);

      String fileId = fileStorage.storing()
                                 .data(content1.getBytes(UTF_8))
                                 .param("uploadBy", "user")
                                 .param("someKeyName", "something")
                                 .param("key", "value")
                                 .param("up by", "tima")
                                 .store();

      System.out.println("fileId = " + fileId);

      byte[] bytes = fileStorage.read(fileId).dataAsArray();

      String content2 = new String(bytes, UTF_8);

      System.out.println(content1);
      System.out.println(content2);
      System.out.println(fileStorage.read(fileId).paramValue("uploadBy"));
      System.out.println(fileStorage.read(fileId).paramValue("up by"));
      System.out.println(fileStorage.read(fileId).allParams());
    }
  }

}
