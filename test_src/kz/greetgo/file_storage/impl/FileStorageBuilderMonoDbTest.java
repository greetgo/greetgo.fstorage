package kz.greetgo.file_storage.impl;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import kz.greetgo.db.DbType;
import kz.greetgo.file_storage.FileDataReader;
import kz.greetgo.file_storage.FileStorage;
import kz.greetgo.file_storage.errors.FileIdAlreadyExists;
import kz.greetgo.file_storage.errors.NoFileData;
import kz.greetgo.file_storage.errors.NoFileMimeType;
import kz.greetgo.file_storage.errors.NoFileName;
import kz.greetgo.file_storage.errors.NoFileWithId;
import kz.greetgo.file_storage.errors.NoParam;
import kz.greetgo.file_storage.errors.Ora00972_IdentifierIsTooLong;
import kz.greetgo.file_storage.errors.UnknownMimeType;
import kz.greetgo.file_storage.impl.util.RND;
import kz.greetgo.file_storage.impl.util.TestUtil;
import org.fest.assertions.api.Assertions;
import org.fest.assertions.data.MapEntry;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.fest.assertions.api.Assertions.assertThat;

public class FileStorageBuilderMonoDbTest extends DataProvidersForTests {

  public static final String SCHEMA = "fs2";

  @Override
  protected boolean traceSql() {
    return false;
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void store_read(DbType dbType) {
    checkDbType(dbType);

    if (dbType == DbType.Oracle) {// TODO pompei нужно доделать для оракла
      throw new SkipException("qLLch2F11t :: Oracle skip");
    }

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .build();

    String data           = "Содержимое " + RND.str(400);
    String name           = RND.str(10);
    Date   lastModifiedAt = RND.dateDays(-10000, -10);

    //
    //
    String fileId = storage.storing()
                           .name(name)
                           .createdAt(lastModifiedAt)
                           .data(data.getBytes(StandardCharsets.UTF_8))
                           .param("newParam", "some")
                           .store();
    FileDataReader reader = storage.read(fileId);
    //
    //

    assertThat(fileId).isNotNull();
    assertThat(reader).isNotNull();
    assertThat(new String(reader.dataAsArray(), StandardCharsets.UTF_8)).isEqualTo(data);
    assertThat(reader.name()).isEqualTo(name);
    assertThat(reader.createdAt()).isEqualTo(lastModifiedAt);
    assertThat(reader.id()).isEqualTo(fileId);

    String name2 = RND.str(10);

    //
    //
    //
    String fileId2 = storage.storing()
                            .name(name2)
                            .data(new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8)))
                            .store();
    FileDataReader reader2 = storage.read(fileId2);
    //
    //
    //

    assertThat(fileId2).isNotNull();
    assertThat(reader2).isNotNull();
    assertThat(new String(reader2.dataAsArray(), StandardCharsets.UTF_8)).isEqualTo(data);
    assertThat(reader2.name()).isEqualTo(name2);
    assertThat(reader2.createdAt()).isNotNull();
    assertThat(reader2.id()).isEqualTo(fileId2);

    assertThat(fileId).isNotEqualTo(fileId2);

    assertThat(reader.allParams()).hasSize(1);
    assertThat(reader.allParams()).contains(MapEntry.entry("newParam", "some"));

    assertThat(reader.paramValue("newParam")).isEqualTo("some");
  }

  @Test(expectedExceptions = NoFileWithId.class, dataProvider = "dbTypeDataProvider")
  public void noId_immediately(DbType dbType) {
    checkDbType(dbType);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .build();

    //
    //
    storage.read(RND.str(10));
    //
    //

  }

  @Test(expectedExceptions = NoFileWithId.class, dataProvider = "dbTypeDataProvider")
  public void noId(DbType dbType) {
    checkDbType(dbType);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .build();

    //
    //
    storage.read(RND.str(10)).dataAsArray();
    //
    //

  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void readOrNull_noId_null(DbType dbType) {
    checkDbType(dbType);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .build();

    //
    //
    FileDataReader dataReader = storage.readOrNull(RND.str(10));
    //
    //

    assertThat(dataReader).isNull();
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void externalIdGenerator(DbType dbType) {
    checkDbType(dbType);
    String prefix = RND.str(5);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .setIdGenerator(20, () -> prefix + RND.str(5))
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setFileParameterTable("file_param_" + prefix)
                                            .build();

    //
    //
    String fileId = storage.storing()
                           .name(RND.str(10))
                           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
                           .store();
    FileDataReader reader = storage.read(fileId);
    //
    //

    assertThat(fileId).isNotNull();
    assertThat(reader).isNotNull();
    assertThat(reader.id()).isEqualTo(fileId);
    assertThat(fileId).startsWith(prefix);
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void bigExternalId(DbType dbType) {
    checkDbType(dbType);
    String prefix = RND.str(50);

    if (dbType == DbType.Oracle) {// TODO pompei нужно доделать для оракла
      throw new SkipException("emA0MwTVT1 :: Oracle skip");
    }

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .setIdGenerator(200, () -> prefix + RND.str(50))
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setDataTable("idLen200_data")
                                            .setDataTableId("file_id")
                                            .setDataTableData("file_data")
                                            .setParamsTable("idLen200_param")
                                            .setParamsTableId("param_id")
                                            .setParamsTableName("param_name")
                                            .setParamsTableNameLength(250)
                                            .setParamsTableDataId("param_data_id")
                                            .setParamsTableMimeType("param_mt")
                                            .setParamsTableMimeTypeLength(450)
                                            .setParamsTableLastModifiedAt("updatedAt")

                                            .build();

    //
    //
    String fileId = storage.storing()
                           .name(RND.str(10))
                           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
                           .param("new Param", "someValue")
                           .param("another Param", "value 2")
                           .store();
    FileDataReader reader = storage.read(fileId);
    //
    //

    assertThat(fileId).isNotNull();
    assertThat(reader).isNotNull();
    assertThat(reader.id()).isEqualTo(fileId);
    assertThat(fileId).startsWith(prefix);
    assertThat(reader.allParams()).hasSize(2);
    assertThat(reader.allParams()).contains(MapEntry.entry("new Param", "someValue"));
    assertThat(reader.allParams()).contains(MapEntry.entry("another Param", "value 2"));
    assertThat(reader.paramValue("another Param")).isEqualTo("value 2");
    assertThat(reader.paramValue("new Param")).isEqualTo("someValue");
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void store_read_presetId(DbType dbType) {
    checkDbType(dbType);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .build();

    String data           = "Содержимое " + RND.str(400);
    String name           = RND.str(10);
    Date   lastModifiedAt = RND.dateDays(-10000, -10);

    String expectedFileId = RND.str(10);

    //
    //
    String fileId = storage.storing()
                           .name(name)
                           .createdAt(lastModifiedAt)
                           .data(data.getBytes(StandardCharsets.UTF_8))
                           .presetId(expectedFileId)
                           .store();
    FileDataReader reader = storage.read(fileId);
    //
    //

    assertThat(fileId).isEqualTo(expectedFileId);
    assertThat(reader).isNotNull();
    assertThat(new String(reader.dataAsArray(), StandardCharsets.UTF_8)).isEqualTo(data);
    assertThat(reader.name()).isEqualTo(name);
    assertThat(reader.createdAt()).isEqualTo(lastModifiedAt);
    assertThat(reader.id()).isEqualTo(fileId);
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void testFileIdAlreadyExists(DbType dbType) {
    checkDbType(dbType);

    FileStorage storage = FileStorageBuilder
      .newBuilder()
      .inDb(TestUtil.createFrom(dbType, SCHEMA))
      .build();

    String fileId = storage.storing()
                           .name(RND.str(10))
                           .data(RND.str(10).getBytes(StandardCharsets.UTF_8))
                           .store();

    try {
      //
      //
      storage.storing()
             .name(RND.str(10))
             .data(RND.str(10).getBytes(StandardCharsets.UTF_8))
             .presetId(fileId)
             .store();
      //
      //

      Assertions.fail("Cannot add some files with same id: fileId = " + fileId);
    } catch (FileIdAlreadyExists e) {
      assertThat(e.fileId).isEqualTo(fileId);
    }
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkMimeTypeWithCustomLength(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setDataTable("usingMimeType_data_" + rnd)
                                            .setParamsTable("usingMimeType_param_" + rnd)
                                            .setFileParameterTable("file_param_" + rnd)
                                            .setParamsTableMimeType("asd_mime_type_" + rnd)
                                            .setParamsTableMimeTypeLength(1000)
                                            .build();

    String mimeType = RND.str(500);

    //
    //
    String fileId = storage.storing()
                           .name(RND.str(10))
                           .mimeType(mimeType)
                           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
                           .param("created by", "user")
                           .param("upload by", "iPhone XR")
                           .store();
    FileDataReader reader = storage.read(fileId);
    //
    //

    assertThat(fileId).isNotNull();
    assertThat(reader).isNotNull();
    assertThat(reader.id()).isEqualTo(fileId);
    assertThat(reader.mimeType()).isEqualTo(mimeType);
    assertThat(reader.allParams()).hasSize(2);
    assertThat(reader.paramValue("upload by")).isEqualTo("iPhone XR");
    assertThat(reader.paramValue("created by")).isEqualTo("user");
  }

  @DataProvider
  public Object[][] nullAndEmpty() {

    List<Object[]> ret = new ArrayList<>();
    for (DbType dbType : dbTypes()) {
      ret.add(new Object[]{dbType, null});
      ret.add(new Object[]{dbType, ""});
    }
    return ret.toArray(new Object[ret.size()][]);
  }

  @Test(expectedExceptions = NoFileMimeType.class, dataProvider = "nullAndEmpty")
  public void checkMimeTypeMandatory(DbType dbType, String nullAndEmpty) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .mandatoryMimeType(true)
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setDataTable("NoFileMimeType_data_" + rnd)
                                            .setParamsTable("NoFileMimeType_param_" + rnd)
                                            .setParamsTableMimeType("asd_mime_type_" + rnd)
                                            .build();

    //
    //
    storage.storing()
           .mimeType(nullAndEmpty)
           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
           .store();
    //
    //
  }

  @Test(expectedExceptions = NoFileName.class, dataProvider = "nullAndEmpty")
  public void checkNameMandatory(DbType dbType, String nullAndEmpty) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .mandatoryName(true)
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setDataTable("NoFileName_data_" + rnd)
                                            .setParamsTable("NoFileName_param_" + rnd)
                                            .build();

    //
    //
    storage.storing()
           .name(nullAndEmpty)
           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
           .store();
    //
    //
  }

  @Test(expectedExceptions = Ora00972_IdentifierIsTooLong.class, enabled = false)// TODO pompei активировать для Оракла
  public void throw_Ora00972_IdentifierIsTooLong_setDataTable() {
    String rnd = RND.intStr(17);

    FileStorageBuilder.newBuilder()
                      .inDb(TestUtil.createFrom(DbType.Oracle, SCHEMA))
                      .setDataTable("name_is_very_very_very_long_" + rnd)
                      .build();
  }

  @Test(expectedExceptions = Ora00972_IdentifierIsTooLong.class, enabled = false)// TODO pompei активировать для Оракла
  public void throw_Ora00972_IdentifierIsTooLong_setParamsTableMimeType() {
    String rnd = RND.intStr(17);

    FileStorageBuilder.newBuilder()
                      .inDb(TestUtil.createFrom(DbType.Oracle, SCHEMA))
                      .setParamsTableMimeType("name_is_very_very_very_long_" + rnd)
                      .build();
  }

  @Test(expectedExceptions = Ora00972_IdentifierIsTooLong.class, enabled = false)// TODO pompei активировать для Оракла
  public void throw_Ora00972_IdentifierIsTooLong_setParamsTable() {
    String rnd = RND.intStr(17);

    FileStorageBuilder.newBuilder()
                      .inDb(TestUtil.createFrom(DbType.Oracle, SCHEMA))
                      .setParamsTable("name_is_very_very_very_long_" + rnd)
                      .build();
  }

  @Test(expectedExceptions = NoFileData.class, dataProvider = "dbTypeDataProvider")
  public void checkDataMandatory(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setDataTable("NoFileContent_data_" + rnd)
                                            .setParamsTable("NoFileContent_param_" + rnd)
                                            .build();

    //
    //
    storage.storing().store();
    //
    //
  }


  @Test(dataProvider = "dbTypeDataProvider")
  public void unknownMimeType(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    String[] actualMimeType = new String[]{null};

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .mimeTypeValidator(hereMimeType -> {
                                              actualMimeType[0] = hereMimeType;
                                              return false;
                                            })
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setDataTable("NoFileContent_data_" + rnd)
                                            .setParamsTable("NoFileContent_param_" + rnd)
                                            .build();

    String mimeType = RND.str(10);

    try {

      //
      //
      storage.storing()
             .mimeType(mimeType)
             .store();
      //
      //

      Assertions.fail("Must to validate MIME type");
    } catch (UnknownMimeType e) {
      assertThat(e.mimeType).isEqualTo(mimeType);
    }

    assertThat(actualMimeType[0]).isEqualTo(mimeType);
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void unknownMimeType_throwsSomeError(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    String[] actualMimeType = new String[]{null};
    String   errorMessage   = RND.str(10);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .mimeTypeValidator(hereMimeType -> {
                                              actualMimeType[0] = hereMimeType;
                                              throw new RuntimeException(errorMessage);
                                            })
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setDataTable("NoFileContent_data_" + rnd)
                                            .setParamsTable("NoFileContent_param_" + rnd)
                                            .build();

    String mimeType = RND.str(10);

    try {

      //
      //
      storage.storing()
             .mimeType(mimeType)
             .store();
      //
      //

      Assertions.fail("Must to validate MIME type");
    } catch (UnknownMimeType e) {
      assertThat(e.mimeType).isEqualTo(mimeType);
      assertThat(e.getCause().getMessage()).isEqualTo(errorMessage);
    }

    assertThat(actualMimeType[0]).isEqualTo(mimeType);
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void unknownMimeType_throwsUnknownMimeType(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    String[] actualMimeType = new String[]{null};
    String   errorMessage   = RND.str(10);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .mimeTypeValidator(hereMimeType -> {
                                              actualMimeType[0] = hereMimeType;
                                              throw new UnknownMimeType(hereMimeType, errorMessage);
                                            })
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setDataTable("NoFileContent_data_" + rnd)
                                            .setParamsTable("NoFileContent_param_" + rnd)
                                            .build();

    String mimeType = RND.str(10);

    try {

      //
      //
      storage.storing()
             .mimeType(mimeType)
             .store();
      //
      //

      Assertions.fail("Must to validate MIME type");
    } catch (UnknownMimeType e) {
      assertThat(e.mimeType).isEqualTo(mimeType);
      assertThat(e.getMessage()).isEqualTo(errorMessage);
    }

    assertThat(actualMimeType[0]).isEqualTo(mimeType);
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkDefaultValue_dataTable(DbType dbType) {
    checkDbType(dbType);
    FileStorageBuilderMonoDb builder = FileStorageBuilder.newBuilder()
                                                         .inDb(TestUtil.createFrom(dbType, SCHEMA));

    assertThat(builder.getDataTable()).isEqualTo("file_storage_data");
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkDefaultValue_dataTableId(DbType dbType) {
    checkDbType(dbType);
    FileStorageBuilderMonoDb builder = FileStorageBuilder.newBuilder()
                                                         .inDb(TestUtil.createFrom(dbType, SCHEMA));

    assertThat(builder.getDataTableId()).isEqualTo("sha1sum");
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkDefaultValue_dataTableData(DbType dbType) {
    checkDbType(dbType);
    FileStorageBuilderMonoDb builder = FileStorageBuilder.newBuilder()
                                                         .inDb(TestUtil.createFrom(dbType, SCHEMA));

    assertThat(builder.getDataTableData()).isEqualTo("data");
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkDefaultValue_paramsTable(DbType dbType) {
    checkDbType(dbType);
    FileStorageBuilderMonoDb builder = FileStorageBuilder.newBuilder()
                                                         .inDb(TestUtil.createFrom(dbType, SCHEMA));

    assertThat(builder.getParamsTable()).isEqualTo("file_storage_params");
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkDefaultValue_paramsTableId(DbType dbType) {
    checkDbType(dbType);
    FileStorageBuilderMonoDb builder = FileStorageBuilder.newBuilder()
                                                         .inDb(TestUtil.createFrom(dbType, SCHEMA));

    assertThat(builder.getParamsTableId()).isEqualTo("id");
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkDefaultValue_paramsTableDataId(DbType dbType) {
    checkDbType(dbType);
    FileStorageBuilderMonoDb builder = FileStorageBuilder.newBuilder()
                                                         .inDb(TestUtil.createFrom(dbType, SCHEMA));

    assertThat(builder.getParamsTableDataId()).isEqualTo("sha1sum");
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkDefaultValue_paramsTableLastModifiedAt(DbType dbType) {
    checkDbType(dbType);
    FileStorageBuilderMonoDb builder = FileStorageBuilder.newBuilder()
                                                         .inDb(TestUtil.createFrom(dbType, SCHEMA));

    assertThat(builder.getParamsTableLastModifiedAt()).isEqualTo("lastModifiedAt");
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkDefaultValue_paramsTableMimeType(DbType dbType) {
    checkDbType(dbType);
    FileStorageBuilderMonoDb builder = FileStorageBuilder.newBuilder()
                                                         .inDb(TestUtil.createFrom(dbType, SCHEMA));

    assertThat(builder.getParamsTableMimeType()).isEqualTo("mimeType");
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkDefaultValue_paramsTableMimeTypeLength(DbType dbType) {
    checkDbType(dbType);
    FileStorageBuilderMonoDb builder = FileStorageBuilder.newBuilder()
                                                         .inDb(TestUtil.createFrom(dbType, SCHEMA));

    assertThat(builder.getParamsTableMimeTypeLength()).isEqualTo(50);
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkDefaultValue_paramsTableName(DbType dbType) {
    checkDbType(dbType);
    FileStorageBuilderMonoDb builder = FileStorageBuilder.newBuilder()
                                                         .inDb(TestUtil.createFrom(dbType, SCHEMA));

    assertThat(builder.getParamsTableName()).isEqualTo("name");
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void checkDefaultValue_paramsTableNameLength(DbType dbType) {
    checkDbType(dbType);
    FileStorageBuilderMonoDb builder = FileStorageBuilder.newBuilder()
                                                         .inDb(TestUtil.createFrom(dbType, SCHEMA));

    assertThat(builder.getParamsTableNameLength()).isEqualTo(300);
  }

  @Test(dataProvider = "dbTypeDataProvider")
  public void deleteFilesInParallel(DbType dbType) throws Throwable {
    checkDbType(dbType);
    String suffix = RND.intStr(10);
    FileStorage fileStorage = FileStorageBuilder.newBuilder()
                                                .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                                .setDataTable("data_" + suffix)
                                                .setParamsTable("par_" + suffix)
                                                .setFileParameterTable("file_param_" + suffix)
                                                .build();

    class DeleteThread extends Thread {

      private final String        fileId;
      private final AtomicInteger okCount;
      private final AtomicInteger errCount;

      public DeleteThread(String fileId, AtomicInteger okCount, AtomicInteger errCount) {
        this.fileId   = fileId;
        this.okCount  = okCount;
        this.errCount = errCount;
      }

      Throwable error = null;

      @Override
      public void run() {
        try {
          fileStorage.delete(fileId);
          okCount.incrementAndGet();
        } catch (Throwable error) {
          if (!(error instanceof NoFileWithId)) {
            this.error = error;
          }

          errCount.incrementAndGet();
        }
      }
    }

    List<String> fileIdList = new ArrayList<>();

    for (int i = 0; i < 30; i++) {

      String fileId = fileStorage.storing()
                                 .data(("for deletion" + RND.str(1000)).getBytes(UTF_8))
                                 .name("File # " + i)
                                 .store();

      fileIdList.add(fileId);
    }

    for (String fileId : fileIdList) {

      AtomicInteger okCount  = new AtomicInteger(0);
      AtomicInteger errCount = new AtomicInteger(0);

      DeleteThread[] threads = new DeleteThread[4];

      for (int i = 0; i < threads.length; i++) {
        threads[i] = new DeleteThread(fileId, okCount, errCount);
      }
      for (DeleteThread thread : threads) {
        thread.start();
      }
      for (DeleteThread thread : threads) {
        thread.join();
        if (thread.error != null) {
          thread.error.printStackTrace();
        }
      }
      for (DeleteThread thread : threads) {
        if (thread.error != null) {
          throw thread.error;
        }
      }

      assertThat(okCount.get()).isEqualTo(1);
      assertThat(errCount.get()).isEqualTo(3);
    }

  }

  @Test(dataProvider = "dbTypeDataProvider", expectedExceptions = NoFileWithId.class)
  public void deleteAbsentFile(DbType dbType) {
    checkDbType(dbType);
    String suffix = RND.intStr(10);
    FileStorage fileStorage = FileStorageBuilder.newBuilder()
                                                .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                                .setDataTable("data1_" + suffix)
                                                .setParamsTable("par1_" + suffix)
                                                .build();


    fileStorage.delete(RND.str(10));
  }

  @Test(dataProvider = "dbTypeDataProvider", expectedExceptions = NoParam.class)
  public void NoParamTest(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setParamsTable("usingMimeType_param_" + rnd)
                                            .setFileParameterTable("file_param_" + rnd)
                                            .setParamsTableMimeTypeLength(1000)
                                            .build();

    //
    //
    storage.storing()
           .name(RND.str(10))
           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
           .param(null, "user")
           .store();
    //
    //
  }


  @Test(dataProvider = "dbTypeDataProvider", expectedExceptions = NoParam.class)
  public void NoParamTest2(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setParamsTable("usingMimeType_param_" + rnd)
                                            .setFileParameterTable("file_param_" + rnd)
                                            .setParamsTableMimeTypeLength(1000)
                                            .build();

    //
    //
    storage.storing()
           .name(RND.str(10))
           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
           .param("some Param", null)
           .store();
    //
    //
  }


  @Test(dataProvider = "dbTypeDataProvider")
  public void delete_Param1(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setParamsTable("usingMimeType_param_" + rnd)
                                            .setFileParameterTable("file_param_" + rnd)
                                            .setDataTable("file_storage_data_" + rnd)
                                            .setParamsTableMimeTypeLength(1000)
                                            .build();

    //
    //
    String fileId = storage.storing()
                           .name(RND.str(10))
                           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
                           .param("some Param", "something")
                           .store();
    //
    //

    FileDataReader reader = storage.read(fileId);


    assertThat(reader.allParams()).hasSize(1);
    assertThat(reader.allParams()).contains(MapEntry.entry("some Param", "something"));
    assertThat(reader.paramValue("some Param")).isEqualTo("something");


    //
    //
    storage.delete(fileId);
    //
    //


    assertThat(reader.allParams()).isNull();
    assertThat(reader.paramValue("some Param")).isNull();
  }


  @Test(dataProvider = "dbTypeDataProvider")
  public void update__Param(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setParamsTable("usingMimeType_param_" + rnd)
                                            .setFileParameterTable("file_param_" + rnd)
                                            .setDataTable("file_storage_data_" + rnd)
                                            .setParamsTableMimeTypeLength(1000)
                                            .build();

    //
    //
    String fileId = storage.storing()
                           .name(RND.str(10))
                           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
                           .param("some Param", "something")
                           .store();
    //
    //

    FileDataReader reader = storage.read(fileId);


    assertThat(reader.allParams()).hasSize(1);
    assertThat(reader.allParams()).contains(MapEntry.entry("some Param", "something"));
    assertThat(reader.paramValue("some Param")).isEqualTo("something");

    String newFileName = RND.str(10);

    //
    //
    storage.updating().param("some Param", null).name(newFileName).store(fileId);
    //
    //

    FileDataReader reader2 = storage.read(fileId);


    assertThat(reader2.allParams()).isNull();
    assertThat(reader2.paramValue("some Param")).isNull();
    assertThat(reader2.name()).isEqualTo(newFileName);
  }


  @Test(dataProvider = "dbTypeDataProvider")
  public void update__Param2(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setParamsTable("usingMimeType_param_" + rnd)
                                            .setFileParameterTable("file_param_" + rnd)
                                            .setDataTable("file_storage_data_" + rnd)
                                            .setParamsTableMimeTypeLength(1000)
                                            .build();

    //
    //
    String fileId = storage.storing()
                           .name(RND.str(10))
                           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
                           .param("some Param", "something")
                           .store();
    //
    //

    FileDataReader reader = storage.read(fileId);


    assertThat(reader.allParams()).hasSize(1);
    assertThat(reader.allParams()).contains(MapEntry.entry("some Param", "something"));
    assertThat(reader.paramValue("some Param")).isEqualTo("something");


    //
    //
    storage.updating().param("some Param", "new value").store(fileId);
    //
    //


    assertThat(reader.allParams()).contains(MapEntry.entry("some Param", "new value"));
    assertThat(reader.paramValue("some Param")).isEqualTo("new value");
  }


  @Test(dataProvider = "dbTypeDataProvider")
  public void update__MultipleParam(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);

    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setParamsTable("usingMimeType_param_" + rnd)
                                            .setFileParameterTable("file_param_" + rnd)
                                            .setDataTable("file_storage_data_" + rnd)
                                            .setParamsTableMimeTypeLength(1000)
                                            .build();

    //
    //
    String fileId = storage.storing()
                           .name(RND.str(10))
                           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
                           .param("some Param", "something")
                           .param("param1", "something")
                           .param("param2", "something")
                           .store();
    //
    //

    FileDataReader reader = storage.read(fileId);


    assertThat(reader.allParams()).hasSize(3);
    assertThat(reader.allParams()).contains(MapEntry.entry("some Param", "something"));
    assertThat(reader.allParams()).contains(MapEntry.entry("param1", "something"));
    assertThat(reader.allParams()).contains(MapEntry.entry("param2", "something"));
    assertThat(reader.paramValue("some Param")).isEqualTo("something");
    assertThat(reader.paramValue("param1")).isEqualTo("something");
    assertThat(reader.paramValue("param2")).isEqualTo("something");


    //
    //
    storage.updating()
           .param("some Param", "new value")
           .param("param1", "newSomething")
           .param("param2", "newSomething2")
           .store(fileId);
    //
    //


    assertThat(reader.allParams()).hasSize(3);
    assertThat(reader.allParams()).contains(MapEntry.entry("some Param", "new value"));
    assertThat(reader.allParams()).contains(MapEntry.entry("param1", "newSomething"));
    assertThat(reader.allParams()).contains(MapEntry.entry("param2", "newSomething2"));
    assertThat(reader.paramValue("some Param")).isEqualTo("new value");
  }


  @Test(dataProvider = "dbTypeDataProvider")
  public void update__MultipleParamAndDeleting(DbType dbType) {
    checkDbType(dbType);
    String rnd = RND.intStr(7);
    System.out.println("file_param_" + rnd);
    FileStorage storage = FileStorageBuilder.newBuilder()
                                            .inDb(TestUtil.createFrom(dbType, SCHEMA))
                                            .setParamsTable("usingMimeType_param_" + rnd)
                                            .setFileParameterTable("file_param_" + rnd)
                                            .setDataTable("file_storage_data_" + rnd)
                                            .setParamsTableMimeTypeLength(1000)
                                            .build();

    //
    //
    String fileId = storage.storing()
                           .name(RND.str(10))
                           .data(RND.str(4).getBytes(StandardCharsets.UTF_8))
                           .param("some Param", "something")
                           .param("param1", "something")
                           .param("param2", "something")
                           .store();
    //
    //

    FileDataReader reader = storage.read(fileId);


    assertThat(reader.allParams()).hasSize(3);
    assertThat(reader.allParams()).contains(MapEntry.entry("some Param", "something"));
    assertThat(reader.allParams()).contains(MapEntry.entry("param1", "something"));
    assertThat(reader.allParams()).contains(MapEntry.entry("param2", "something"));
    assertThat(reader.paramValue("some Param")).isEqualTo("something");
    assertThat(reader.paramValue("param1")).isEqualTo("something");
    assertThat(reader.paramValue("param2")).isEqualTo("something");


    //
    //
    storage.updating()
           .param("some Param", "new value")
           .param("param1", null)
           .param("param2", "newSomething2")
           .store(fileId);
    //
    //


    assertThat(reader.allParams()).hasSize(2);
    assertThat(reader.allParams()).contains(MapEntry.entry("some Param", "new value"));
    assertThat(reader.allParams()).contains(MapEntry.entry("param2", "newSomething2"));
    assertThat(reader.paramValue("param1")).isNull();
  }
}
