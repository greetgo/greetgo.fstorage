#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 131

bash down.sh
bash start.sh
