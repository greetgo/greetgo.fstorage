#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 131

docker compose down
EXIT="$?"
# shellcheck disable=SC2181
if [ "$EXIT" != "0" ] ; then
  echo "%%%"
  echo "%%% ERROR of : docker compose down"
  echo "%%%"
  exit "$EXIT"
fi
