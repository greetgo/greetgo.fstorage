#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 131

bash down.sh

docker run --rm -v "$HOME/volumes/fstorage/:/data" \
       busybox:1.28 \
       find /data -mindepth 1 -maxdepth 1 -exec \
       rm -rf {} \;
