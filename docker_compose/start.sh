#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 131

docker compose up -d
EXIT="$?"
# shellcheck disable=SC2181
if [ "$EXIT" != "0" ] ; then
  echo "%%%"
  echo "%%% ERROR of : docker compose up -d"
  echo "%%%"
  exit "$EXIT"
fi
