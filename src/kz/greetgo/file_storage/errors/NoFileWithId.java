package kz.greetgo.file_storage.errors;

public class NoFileWithId extends FileStorageError {
  public final String fileId;

  public NoFileWithId(String placeId, String fileId) {
    super(placeId + " :: No file with id = " + fileId);
    this.fileId = fileId;
  }

  public NoFileWithId(String placeId, String fileId, Throwable cause) {
    super(placeId + " :: No file with id = " + fileId, cause);
    this.fileId = fileId;
  }
}
