package kz.greetgo.file_storage.errors;

public class FileStorageError extends RuntimeException {
  public FileStorageError(String message) {
    super(message);
  }

  public FileStorageError(Exception e) {
    super(e);
  }

  public FileStorageError(String message, Throwable e) {
    super(message, e);
  }
}
