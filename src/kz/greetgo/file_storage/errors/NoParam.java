package kz.greetgo.file_storage.errors;

public class NoParam extends RuntimeException{
  public NoParam(){
    super("Parameter name is not present!");
  }
}
