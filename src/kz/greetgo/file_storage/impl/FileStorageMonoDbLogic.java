package kz.greetgo.file_storage.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import kz.greetgo.file_storage.FileDataReader;
import kz.greetgo.file_storage.FileStorage;
import kz.greetgo.file_storage.FileStoringOperation;
import kz.greetgo.file_storage.FileUpdatingOperation;
import kz.greetgo.file_storage.errors.NoFileData;
import kz.greetgo.file_storage.errors.NoFileWithId;
import kz.greetgo.file_storage.errors.NoParam;

public class FileStorageMonoDbLogic implements FileStorage {

  private final FileStorageBuilderImpl builder;
  private final MonoDbOperations       monoDbOperations;

  FileStorageMonoDbLogic(FileStorageBuilderImpl builder, MonoDbOperations monoDbOperations) {
    this.builder          = builder;
    this.monoDbOperations = monoDbOperations;
  }

  @Override
  public FileStoringOperation storing() {
    return new FileStoringOperation() {
      final CreateNewParams params = new CreateNewParams();

      @Override
      public FileStoringOperation name(String name) {
        params.name = name;

        {
          Function<String, String> mimeTypeExtractor = builder.mimeTypeExtractor;
          if (mimeTypeExtractor != null) {
            params.mimeType = mimeTypeExtractor.apply(name);
          }
        }

        return this;
      }

      @Override
      public FileStoringOperation mimeType(String mimeType) {
        params.mimeType = mimeType;
        return this;
      }

      @Override
      public FileStoringOperation createdAt(Date createdAt) {
        params.createdAt = createdAt;
        return this;
      }

      byte[]      data        = null;
      InputStream inputStream = null;

      @Override
      public FileStoringOperation data(byte[] data) {
        checkSetData();
        this.data = data != null ? data : new byte[0];
        return this;
      }

      private void checkSetData() {
        if (data != null || inputStream != null) {
          throw new IllegalStateException("data already defined");
        }
      }

      @Override
      public FileStoringOperation data(InputStream inputStream) {
        if (inputStream == null) {
          throw new IllegalArgumentException("inputStream == null");
        }
        checkSetData();
        this.inputStream = inputStream;
        return this;
      }

      private byte[] getData() {
        if (data != null) {
          return data;
        }
        if (inputStream != null) {
          return LocalUtil.readAll(inputStream);
        }
        throw new NoFileData();
      }


      @Override
      public FileStoringOperation presetId(String presetFileId) {
        params.presetFileId = presetFileId;
        return this;
      }

      @Override
      public FileStoringOperation param(String paramName, String paramValue) {
        if (paramName == null || paramValue == null) {
          throw new NoParam();
        }
        params.param.put(paramName, paramValue);
        return this;
      }

      @Override
      public String store() {
        builder.checkName(params.name);
        builder.checkMimeType(params.mimeType);
        try {
          return monoDbOperations.createNew(getData(), params);
        } catch (DatabaseNotPrepared databaseNotPrepared) {
          monoDbOperations.prepareDatabase(databaseNotPrepared);
          return monoDbOperations.createNew(getData(), params);
        }
      }
    };
  }

  @Override
  public FileUpdatingOperation updating() {

    return new FileUpdatingOperation() {

      boolean isNameSet = false;
      String  newName   = null;

      Map<String, String> newParam = new HashMap<>();

      @Override
      public FileUpdatingOperation name(String name) {
        isNameSet = true;
        newName   = name;
        return this;
      }

      @Override
      public FileUpdatingOperation param(String name, String newValue) {
        if (name != null) {
          newParam.put(name, newValue);
        }
        return this;
      }

      @Override
      public void store(String fileId) {
        if (!newParam.isEmpty()) {
          monoDbOperations.updateParam(fileId, newParam);
        }
        if (isNameSet) {
          monoDbOperations.updateFileName(fileId, newName);
        }
      }

    };

  }


  @Override
  public FileDataReader read(String fileId) throws NoFileWithId {

    FileDataReader reader = readOrNull(fileId);

    if (reader == null) {
      throw new NoFileWithId("fX11G2y10t", fileId);
    }

    return reader;
  }

  @Override
  public FileDataReader readOrNull(String fileId) {
    final FileParams params = monoDbOperations.readParams(fileId);

    if (params == null) {
      return null;
    }

    return new FileDataReader() {
      @Override
      public String name() {
        return params.name;
      }

      byte[] data = null;

      final Object sync = new Object();

      @Override
      public byte[] dataAsArray() {
        {
          byte[] data = this.data;
          if (data != null) {
            return data;
          }
        }

        synchronized (sync) {
          {
            byte[] data = this.data;
            if (data != null) {
              return data;
            }
          }

          return data = monoDbOperations.getDataAsArray(params.sha1sum);
        }
      }

      @Override
      public Date createdAt() {
        return params.createdAt;
      }

      @Override
      public String mimeType() {
        return params.mimeType;
      }

      @Override
      public String paramValue(String paramName) {
        return getFileParameter(fileId, paramName);
      }

      @Override
      public Map<String, String> allParams() {
        return getAllParameters(fileId);
      }

      @Override
      public String id() {
        return params.id;
      }

      @Override
      public void writeTo(OutputStream out) {
        try {
          out.write(dataAsArray());
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    };
  }

  @Override
  public void delete(String fileId) throws NoFileWithId {
    monoDbOperations.delete(fileId);
  }

  private String getFileParameter(String fileId, String paramName) {
    return monoDbOperations.getFileParams(fileId, paramName);
  }

  private Map<String, String> getAllParameters(String fileId) {
    return monoDbOperations.getAllParams(fileId);
  }
}
