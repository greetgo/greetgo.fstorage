package kz.greetgo.file_storage.impl;

import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSFindIterable;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.mongodb.client.gridfs.model.GridFSUploadOptions;
import com.mongodb.client.model.Updates;
import com.mongodb.lang.NonNull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import kz.greetgo.file_storage.FileDataReader;
import kz.greetgo.file_storage.FileStorage;
import kz.greetgo.file_storage.FileStoringOperation;
import kz.greetgo.file_storage.FileUpdatingOperation;
import kz.greetgo.file_storage.errors.NoFileMimeType;
import kz.greetgo.file_storage.errors.NoFileName;
import kz.greetgo.file_storage.errors.NoFileWithId;
import kz.greetgo.file_storage.errors.NoParam;
import org.bson.BsonObjectId;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static kz.greetgo.file_storage.impl.IdGeneratorType.HEX12;

public class FileStorageMongoGridFs implements FileStorage {
  private final       FileStorageBuilderInMongoGridFsImpl builder;
  private final       GridFSBucket                        bucket;
  public static final String                              MORE = "more";

  public FileStorageMongoGridFs(FileStorageBuilderInMongoGridFsImpl builder) {
    this.builder = builder;
    bucket       = createBucket();
  }

  private @NonNull GridFSBucket createBucket() {
    String bucketName = builder.bucketName;
    if (bucketName == null || bucketName.trim().isEmpty()) {
      return GridFSBuckets.create(builder.database);
    }
    return GridFSBuckets.create(builder.database, bucketName);
  }

  @Override
  public FileStoringOperation storing() {

    return new FileStoringOperation() {
      String fileName = null;

      @Override
      public FileStoringOperation name(String name) {
        this.fileName = name;

        Function<String, String> mimeTypeExtractor = builder.parent.mimeTypeExtractor;
        if (mimeTypeExtractor != null) {
          mimeType = mimeTypeExtractor.apply(name);
        }

        return this;
      }

      Date createdAt = null;

      @Override
      public FileStoringOperation createdAt(Date createdAt) {
        this.createdAt = createdAt;
        return this;
      }

      String mimeType = null;

      @Override
      public FileStoringOperation mimeType(String mimeType) {
        builder.parent.validateMimeType(mimeType);
        this.mimeType = mimeType;
        return this;
      }

      byte[] data = null;

      @Override
      public FileStoringOperation data(byte[] data) {
        Objects.requireNonNull(data);
        this.data   = data;
        inputStream = null;
        return this;
      }

      InputStream inputStream = null;

      @Override
      public FileStoringOperation data(InputStream inputStream) {
        Objects.requireNonNull(inputStream);
        this.inputStream = inputStream;
        data             = null;
        return this;
      }

      String presetFileId = null;

      @Override
      public FileStoringOperation presetId(String presetFileId) {
        this.presetFileId = presetFileId;
        return this;
      }

      final Map<String, String> param = new HashMap<>();

      @Override
      public FileStoringOperation param(String paramName, String paramValue) {
        if (paramName.isEmpty() || paramValue.isEmpty()) {
          throw new NoParam();
        }
        param.put(paramName, paramValue);
        return this;
      }

      @Override
      public String store() {
        return storeFile(fileName, createdAt, mimeType, data, inputStream, presetFileId, param);
      }
    };
  }

  @Override
  public FileUpdatingOperation updating() {
    return new FileUpdatingOperation() {

      boolean isNameSet = false;
      String  newName   = null;

      @Override
      public FileUpdatingOperation name(String name) {
        isNameSet = true;
        newName   = name;
        return this;
      }

      Map<String, String> param;

      @Override
      public FileUpdatingOperation param(String name, String newValue) {
        if (name != null) {
          if (param == null) {
            param = new HashMap<>();
          }
          param.put(name, newValue);
        }
        return this;
      }

      @Override
      public void store(String fileId) {

        Objects.requireNonNull(fileId, "File ID is expected to be not null");

        if (isNameSet) {
          builder.parent.checkName(newName);

          BsonValue bsonId = convertToBsonId(fileId);

          bucket.rename(bsonId, newName);
        }

        {
          final Map<String, String> param = this.param;
          if (param != null) {
            BsonValue bsonId = convertToBsonId(fileId);

            List<Bson> updates = new ArrayList<>();
            for (final Map.Entry<String, String> e : param.entrySet()) {
              final String paramName  = e.getKey();
              final String paramValue = e.getValue();
              updates.add(Updates.set("metadata." + MORE + '.' + paramName, paramValue));
            }

            if (updates.size() > 0) {
              builder.database.getCollection(bucket.getBucketName() + ".files")
                              .updateOne(eq("_id", bsonId), combine(updates));
            }

          }
        }

      }

    };
  }

  private String storeFile(String fileName, Date createdAt, String mimeType,
                           byte[] data, InputStream inputStreamArg, String presetFileId, Map<String, String> param) {

    HashMap<String, Object> metadata = new HashMap<>();
    metadata.put("createdAt", createdAt != null ? createdAt : new Date());
    if (mimeType != null) {
      metadata.put("mimeType", mimeType);
    } else if (builder.parent.mandatoryMimeType) {
      throw new NoFileMimeType();
    }

    if (param != null) {
      metadata.put(MORE, param);
    }

    if (fileName == null && builder.parent.mandatoryName) {
      throw new NoFileName();
    }

    GridFSUploadOptions options = new GridFSUploadOptions().chunkSizeBytes(builder.chunkSizeBytes)
                                                           .metadata(new Document(metadata));

    String id = presetFileId;
    if (id == null || id.trim().isEmpty()) {
      id = builder.parent.idGenerator(HEX12).get();
    }

    BsonValue bsonId = convertToBsonId(id);

    InputStream inputStream = inputStreamArg;
    if (inputStream == null) {
      inputStream = new ByteArrayInputStream(Objects.requireNonNull(data, "data - array of bytes - content of file"));
    }

    if (fileName == null) {
      fileName = "default-name";
    }

    bucket.uploadFromStream(bsonId, fileName, inputStream, options);

    return id;
  }

  private @NonNull BsonValue convertToBsonId(String id) {

    if (builder.useObjectId) {

      byte[] idBytes;
      try {
        idBytes = HexUtil.hexToBytes(id);
      } catch (HexUtil.HexConvertException e) {
        throw new IllegalId("FJNv1e39J0 :: When useObjectId == true, then id must be hex string"
                              + " for 12 bytes: " + e.getMessage(), e);
      }

      if (idBytes.length != 12) {
        throw new IllegalId("When useObjectId == true, then id must be hex string for 12 bytes,"
                              + " but now length = " + idBytes.length + " : id = `" + id + "`");
      }

      ObjectId objectId = new ObjectId(idBytes);

      return new BsonObjectId(objectId);

    } else {

      return new BsonString(id);

    }
  }

  @Override
  public FileDataReader read(String fileId) throws NoFileWithId {
    FileDataReader reader = readOrNull(fileId);

    if (reader == null) {
      throw new NoFileWithId("2pYLJ5lC0g", fileId);
    }

    return reader;
  }

  @Override
  public void delete(String fileId) throws NoFileWithId {
    try {
      bucket.delete(convertToBsonId(fileId));
    } catch (IllegalId e) {
      throw new NoFileWithId("7mZiWVI0DP", fileId, e);
    }
  }

  @Override
  public FileDataReader readOrNull(String fileId) {

    BsonValue bsonId = convertToBsonId(fileId);

    GridFSFindIterable iterable = bucket.find(eq("_id", bsonId));
    GridFSFile         file     = iterable.first();

    if (file == null) {
      return null;
    }

    return new FileDataReader() {
      @Override
      public String name() {
        return file.getFilename();
      }

      @Override
      public byte[] dataAsArray() {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        writeTo(bOut);
        return bOut.toByteArray();
      }

      @Override
      public void writeTo(OutputStream out) {
        bucket.downloadToStream(bsonId, out);
      }

      @Override
      public Date createdAt() {
        Document metadata = file.getMetadata();
        if (metadata == null) {
          return file.getUploadDate();
        }
        Object createdAt = metadata.get("createdAt");
        if (createdAt == null) {
          return file.getUploadDate();
        }
        return (Date) createdAt;
      }

      @Override
      public String mimeType() {
        Document metadata = file.getMetadata();
        if (metadata == null) {
          return null;
        }
        Object mimeType = metadata.get("mimeType");
        if (mimeType == null) {
          return null;
        }
        return (String) mimeType;
      }

      @Override
      public String paramValue(String paramName) {
        Document metadata = file.getMetadata();
        if (metadata == null) {
          return null;
        }
        Document params = (Document) metadata.get("more");
        if (params == null) {
          return null;
        }
        Object value = params.get(paramName);
        if (value == null) {
          return null;
        }
        return (String) value;
      }

      @Override
      public Map<String, String> allParams() {
        Document metadata = file.getMetadata();
        if (metadata == null) {
          return null;
        }
        Document ret = (Document) metadata.get("more");
        return ret.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> (String) e.getValue()));
      }

      @Override
      public String id() {
        return fileId;
      }
    };
  }
}
