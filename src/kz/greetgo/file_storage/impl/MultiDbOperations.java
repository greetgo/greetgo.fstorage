package kz.greetgo.file_storage.impl;

import java.util.Map;
import javax.sql.DataSource;
import kz.greetgo.file_storage.errors.TableIsAbsent;
import kz.greetgo.file_storage.impl.jdbc.insert.Insert;
import kz.greetgo.file_storage.impl.jdbc.structure.Table;

public interface MultiDbOperations {
  void createTableQuiet(DataSource dataSource, Table table);

  void insert(DataSource dataSource, Insert insert, TablePosition tablePosition) throws TableIsAbsent;

  byte[] loadData(DataSource dataSource, String tableName, String idFieldName, String idValue, String gettingFieldName);

  FileParams loadFileParams(DataSource dataSource, String tableName, String fileId, TableFieldNames names);

  void delete(DataSource dataSource, String tableName, String fileIdField, String fileId, TablePosition tablePosition, boolean checkUpdate);

  String loadFileParameter(DataSource dataSource, String tableName, String fileId, String paramName, TableFieldNamesForParam names);

  Map<String, String> loadFileParameters(DataSource dataSource, String tableName, String fileId, TableFieldNamesForParam names);

  void updateParam(String fileId,
                   Map<String, String> param,
                   DataSource dataSource,
                   String tableName,
                   TableFieldNamesForParam names);

  void updateFileName(String fileId, String newFileName, DataSource dataSource);
}
