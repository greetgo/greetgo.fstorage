package kz.greetgo.file_storage.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import javax.sql.DataSource;
import kz.greetgo.file_storage.impl.jdbc.Query;

public class MultiDbOperationsOracle extends MultiDbOperationsPostgres {
  @Override
  protected String strType(int len) {
    return "varchar2(" + len + ")";
  }

  @Override
  protected String timestampType() {
    return "timestamp";
  }

  @Override
  protected String blobType() {
    return "blob";
  }

  @Override
  protected String currentTimestampFunc() {
    return "systimestamp";
  }

  @Override
  public void updateParam(String fileId, Map<String, String> param, DataSource dataSource, String tableName, TableFieldNamesForParam names) {

    try (Connection connection = dataSource.getConnection(); Query query = new Query(connection)) {

      {
        if (!param.isEmpty()) {
          param.forEach((key, value) -> {
            query.sql.append("merge into ").append(tableName).append(" source using (select ");
            query.sql.append("? ").append(names.id).append(", ");
            query.sql.append("? ").append(names.name).append(", ");
            query.sql.append("? ").append(names.value);
            query.sql.append(" from dual) val ");

            query.params.add(fileId);
            query.params.add(key);
            query.params.add(value);

            query.sql.append("on ( ");
            query.sql.append("source.").append(names.id).append(" = ").append("val.").append(names.id);
            query.sql.append(" and ");
            query.sql.append("source.").append(names.name).append(" = ").append("val.").append(names.name);
            query.sql.append(" ) ");

            query.sql.append("when matched then ");
            query.sql.append("update set ");
            query.sql.append("source.").append(names.value).append(" = ").append("val.").append(names.value);

            query.sql.append(" when not matched then ");
            query.sql.append("insert ( ").append(names.id).append(", ").append(names.name).append(", ").append(names.value).append(" ) ");
            query.sql.append("values ( ");
            query.sql.append("val.").append(names.id).append(", ");
            query.sql.append("val.").append(names.name).append(", ");
            query.sql.append("val.").append(names.value);
            query.sql.append(") ");

            try {
              query.update();
            } catch (SQLException e) {
              throw new RuntimeException("e.getSQLState() = " + e.getSQLState() + " :: " + e.getMessage(), e);
            }

          });
        }
      }

    } catch (SQLException e) {
      throw new RuntimeException("e.getSQLState() = " + e.getSQLState() + " :: " + e.getMessage(), e);
    }

  }
}
