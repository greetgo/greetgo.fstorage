package kz.greetgo.file_storage.impl.jdbc.structure;

public class UpdateField {
  public final String fieldName;
  public final Object fieldValue;
  public final String columnName;

  public UpdateField(String fieldName, Object fieldValue, String columnName) {
    this.fieldName  = fieldName;
    this.fieldValue = fieldValue;
    this.columnName = columnName;
  }
}
