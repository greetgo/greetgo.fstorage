package kz.greetgo.file_storage.impl.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import kz.greetgo.file_storage.impl.jdbc.model.Field;
import kz.greetgo.file_storage.impl.jdbc.model.FieldWithExpr;
import kz.greetgo.file_storage.impl.jdbc.model.FieldWithValue;
import kz.greetgo.file_storage.impl.jdbc.structure.UpdateField;

public class Inserting {


  private final Connection               connection;
  private       Function<String, String> sqlPreparation = (s) -> s;

  private Inserting(Connection connection) {
    this.connection = connection;
  }

  public static Inserting with(Connection connection) {
    return new Inserting(connection);
  }

  public Inserting sqlPreparation(Function<String, String> sqlPreparation) {
    this.sqlPreparation = sqlPreparation;
    return this;
  }

  public InsertInto into(String table) {
    return new InsertInto(table);
  }


  public class InsertInto {
    private String            table;
    final   List<Field>       fields         = new ArrayList<>();
    final   List<String>      appendToEnd    = new ArrayList<>();
    final   List<String>      conflictFields = new ArrayList<>();
    final   List<UpdateField> updateFields   = new ArrayList<>();

    public InsertInto(String table) {
      this.table = table;
    }

    public InsertInto field(String fieldName, Object fieldValue) {
      fields.add(new FieldWithValue(fieldName, fieldValue));
      return this;
    }

    public InsertInto onConflict(String conflictFields) {
      this.conflictFields.add(conflictFields);
      return this;
    }

    public InsertInto update(String fieldName, Object value, String columnName) {
      updateFields.add(new UpdateField(fieldName, value, columnName));
      return this;
    }

    @SuppressWarnings("unused")
    public InsertInto fieldSkipNull(String fieldName, Object fieldValue) {
      if (fieldValue != null) {
        fields.add(new FieldWithValue(fieldName, fieldValue));
      }
      return this;
    }

    public InsertInto fieldTimestamp(String fieldName, Date fieldValue, boolean skipNull) {
      Timestamp v = fieldValue == null ? null : new Timestamp(fieldValue.getTime());
      if (!skipNull || v != null) {
        fields.add(new FieldWithValue(fieldName, v));
      }
      return this;
    }

    @SuppressWarnings("unused")
    public InsertInto fieldExpr(String fieldName, String fieldExpression) {
      fields.add(new FieldWithExpr(fieldName, fieldExpression));
      return this;
    }

    @SuppressWarnings("UnusedReturnValue")
    public int go() throws SQLException {

      try (Query query = new Query(connection)) {

        StringBuilder sql = new StringBuilder("insert into " + table + " (");
        sql.append(fields.stream().map(f -> f.name).collect(Collectors.joining(", ")));
        sql.append(") values (");
        sql.append(fields.stream().map(Field::place).collect(Collectors.joining(", ")));
        sql.append(")");

        query.params = fields.stream()
                             .filter(FieldWithValue.class::isInstance)
                             .map(f -> ((FieldWithValue) f).value)
                             .collect(Collectors.toList());

        if (!conflictFields.isEmpty()) {
          sql.append(" on conflict (");

          int conflictSize = conflictFields.size();
          for (int i = 0; i < conflictSize; i++) {
            if (i == conflictSize - 1) {
              sql.append(conflictFields.get(i));
            } else {
              sql.append(conflictFields.get(i)).append(", ");
            }
          }
          sql.append(") do update ");

          boolean first = true;
          for (final UpdateField updateField : updateFields) {
            if (first) {
              sql.append(" set ");
              first = false;
            } else {
              sql.append(", ");
            }
            sql.append(updateField.columnName).append(" = ?");
            query.params.add(updateField.fieldValue);
          }
        }

        appendToEnd.forEach(s -> sql.append(' ').append(s));

        query.sql.append(sqlPreparation.apply(sql.toString()));

        return query.update();
      }

    }

    public InsertInto appendToEnd(String sqlPart) {
      appendToEnd.add(sqlPart);
      return this;
    }
  }

}
