package kz.greetgo.file_storage.impl.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import kz.greetgo.file_storage.impl.jdbc.model.Field;
import kz.greetgo.file_storage.impl.jdbc.model.FieldWithValue;
import kz.greetgo.file_storage.impl.jdbc.structure.UpdateField;

public class Merging {

  private final Connection               connection;
  private       Function<String, String> sqlPreparation = (s) -> s;

  private Merging(Connection connection) {
    this.connection = connection;
  }

  public static Merging with(Connection connection) {
    return new Merging(connection);
  }

  public Merging sqlPreparation(Function<String, String> sqlPreparation) {
    this.sqlPreparation = sqlPreparation;
    return this;
  }

  public Merging.MergingInto into(String table) {
    return new Merging.MergingInto(table);
  }


  public class MergingInto {
    private final String            table;
    final         List<Field>       fields         = new ArrayList<>();
    final         List<String>      appendToEnd    = new ArrayList<>();
    final         List<String>      conflictFields = new ArrayList<>();
    final         List<UpdateField> updateFields   = new ArrayList<>();

    public MergingInto(String table) {
      this.table = table;
    }

    public Merging.MergingInto field(String fieldName, Object fieldValue) {
      fields.add(new FieldWithValue(fieldName, fieldValue));
      return this;
    }

    public Merging.MergingInto onConflict(String conflictFields) {
      this.conflictFields.add(conflictFields);
      return this;
    }

    public Merging.MergingInto update(String fieldName, Object value, String columnName) {
      updateFields.add(new UpdateField(fieldName, value, columnName));
      return this;
    }

    public Merging.MergingInto appendToEnd(String sqlPart) {
      appendToEnd.add(sqlPart);
      return this;
    }

    @SuppressWarnings("UnusedReturnValue")
    public int go() throws SQLException {

      try (Query query = new Query(connection)) {

        StringBuilder sql = new StringBuilder("merge into " + table + " source using (select ");
        sql.append(fields.stream().map(f -> "? " + f.name).collect(Collectors.joining(", ")));
        sql.append(" from dual) val ");

        query.params = fields.stream()
                             .filter(FieldWithValue.class::isInstance)
                             .map(f -> ((FieldWithValue) f).value)
                             .collect(Collectors.toList());

        sql.append("on ( ");

        {
          String sDelimiter = "";

          for (final String f : conflictFields) {
            sql.append(sDelimiter)
               .append("source.")
               .append(f)
               .append(" = ")
               .append("val.")
               .append(f);
            sDelimiter = " and ";
          }
        }

        sql.append(" )");

        sql.append(" when matched then ");
        sql.append(" update set ");

        {
          String sDelimiter = "";
          for (UpdateField updateField : updateFields) {
            sql.append(sDelimiter)
               .append("source.")
               .append(updateField.columnName)
               .append(" = ")
               .append("val.")
               .append(updateField.columnName);
            sDelimiter = " , ";
          }
        }

        sql.append(" when not matched then ");
        sql.append("insert ( ");
        sql.append(fields.stream().map(f -> f.name).collect(Collectors.joining(", ")));
        sql.append(") ");
        sql.append("values ( ");
        sql.append(fields.stream().map(f -> "val." + f.name).collect(Collectors.joining(", ")));
        sql.append(" )");

        appendToEnd.forEach(s -> sql.append(' ').append(s));

        query.sql.append(sqlPreparation.apply(sql.toString()));

        return query.update();
      }

    }

  }

}
