package kz.greetgo.file_storage.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

class CreateNewParams {
  String              name         = null;
  Date                createdAt    = null;
  String              presetFileId = null;
  String              mimeType     = null;
  Map<String, String> param        = new HashMap<>();
}
