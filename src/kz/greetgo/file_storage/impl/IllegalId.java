package kz.greetgo.file_storage.impl;

public class IllegalId extends RuntimeException {
  public IllegalId(String message) {
    super(message);
  }

  public IllegalId(String message, Throwable e) {
    super(message, e);
  }
}
