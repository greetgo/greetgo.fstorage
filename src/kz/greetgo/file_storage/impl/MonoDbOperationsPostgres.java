package kz.greetgo.file_storage.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import kz.greetgo.file_storage.errors.FileIdAlreadyExists;
import kz.greetgo.file_storage.errors.NoFileWithId;
import kz.greetgo.file_storage.impl.jdbc.Inserting;
import kz.greetgo.file_storage.impl.jdbc.Query;

import static kz.greetgo.file_storage.impl.IdGeneratorType.STR13;

public class MonoDbOperationsPostgres extends AbstractMonoDbOperations {
  MonoDbOperationsPostgres(FileStorageBuilderMonoDbImpl builder) {
    super(builder);
  }

  @Override
  public String createNew(byte[] data, CreateNewParams params) throws DatabaseNotPrepared {
    try {
      return createNewEx(data, params);
    } catch (SQLException e) {
      throw prepareException(e);
    }
  }

  private String createNewEx(byte[] data, CreateNewParams params) throws SQLException {

    String sha1sum = sha1sum(data);

    try (Connection connection = builder.dataSource.getConnection()) {

      connection.setAutoCommit(false);
      try {

        Inserting.with(connection)
                 .sqlPreparation(this::sql)
                 .into("__dataTable__")
                 .field("__dataTableId__", sha1sum)
                 .field("__dataTableData__", data)
                 .appendToEnd("ON CONFLICT DO NOTHING")
                 .go()
        ;

        String id = params.presetFileId != null ? params.presetFileId : builder.parent.idGenerator(STR13).get();

        try {
          Inserting.with(connection)
                   .sqlPreparation(this::sql)
                   .into("__paramsTable__")
                   .field("__paramsTableId__", id)
                   .field("__paramsTableName__", params.name)
                   .field("__paramsTableMimeType__", params.mimeType)
                   .field("__paramsTableDataId__", sha1sum)
                   .fieldTimestamp("__paramsTableLastModifiedAt__", params.createdAt, true)
                   .go()
          ;
        } catch (SQLException e) {
          if ("23505".equals(e.getSQLState())) {
            throw new FileIdAlreadyExists(id);
          }
          throw e;
        }

        {
          Map<String, String> strParams = params.param;
          if (strParams != null) {
            for (Map.Entry<String, String> entry : strParams.entrySet()) {
              String key   = entry.getKey();
              String value = entry.getValue();
              try {
                Inserting.with(connection)
                         .sqlPreparation(this::sql)
                         .into("__fileParameterTable__")
                         .field("__fileParameterId__", id)
                         .field("__fileParameterName__", key)
                         .field("__fileParameterValue__", value)
                         .go();
              } catch (SQLException e) {
                if ("23505".equals(e.getSQLState())) {
                  throw new FileIdAlreadyExists(id);
                }
                throw e;
              }
            }
          }
        }

        connection.commit();

        return id;

      } catch (SQLException | RuntimeException e) {
        connection.rollback();
        throw e;
      } finally {
        connection.setAutoCommit(true);
      }
    }

  }

  protected void deleteEx(String fileId) throws SQLException, NoFileWithId {
    try (Connection connection = builder.dataSource.getConnection()) {

      connection.setAutoCommit(false);
      try {

        String sha1sum = loadSha1sumByFileId(connection, fileId);

        doDelete(connection, "delete from __paramsTable__ where __paramsTableId__ = ?", fileId, fileId, true);
        doDelete(connection, "delete from __dataTable__ where __dataTableId__ = ?", sha1sum, fileId, true);

        connection.commit();

        return;

      } catch (SQLException | RuntimeException e) {
        connection.rollback();
        throw e;
      } finally {
        connection.setAutoCommit(true);
      }
    }
  }

  protected void deleteExParam(String fileId) throws SQLException, NoFileWithId {
    try (Connection connection = builder.dataSource.getConnection()) {

      connection.setAutoCommit(false);
      try {

        doDelete(connection, "delete from __fileParameterTable__ where __fileParameterId__ = ?", fileId, fileId, false);

        connection.commit();

        return;

      } catch (SQLException | RuntimeException e) {
        connection.rollback();
        final String message = e.getMessage();
        if (message != null && message.startsWith("ORA-00942:")) {
          return;
        }
        throw e;
      } finally {
        connection.setAutoCommit(true);
      }
    }
  }

  protected void doDelete(Connection connection, String sql, String id, String fileId, boolean changeUpdate) throws SQLException {

    try (PreparedStatement ps = connection.prepareStatement(this.sql(sql))) {

      ps.setString(1, id);

      int updateCount = ps.executeUpdate();

      if (changeUpdate && updateCount < 1) {
        throw new NoFileWithId("85S3gD11yt", fileId);
      }

    }
  }

  private void doDeleteParam(Connection connection, String sql, String fileId, String paramName, boolean changeUpdate) throws SQLException {

    try (PreparedStatement ps = connection.prepareStatement(this.sql(sql))) {

      ps.setString(1, fileId);
      ps.setString(2, paramName);

      int updateCount = ps.executeUpdate();

      if (changeUpdate && updateCount < 1) {
        throw new NoFileWithId("OC9daNPGOl", fileId);
      }

    }
  }

  protected String loadSha1SumSql() {
    return "select __paramsTableDataId__ from __paramsTable__ where __paramsTableId__ = ? for update";
  }

  protected String loadSha1sumByFileId(Connection connection, String fileId) throws SQLException {
    try (PreparedStatement ps = connection.prepareStatement(sql(loadSha1SumSql()))) {
      ps.setString(1, fileId);

      try (ResultSet rs = ps.executeQuery()) {

        if (!rs.next()) {
          throw new NoFileWithId("on1tEv3hN1", fileId);
        }

        return rs.getString(1);
      }

    }
  }

  @Override
  public void delete(String fileId) throws NoFileWithId {
    try {
      deleteEx(fileId);
      deleteExParam(fileId);
    } catch (SQLException e) {
      if (e.getMessage() != null && e.getMessage().startsWith("ORA-00942")) {
        throw new NoFileWithId("4dxLe2jMeV", fileId);
      }
      if ("42P01".equals(e.getSQLState())) {
        throw new NoFileWithId("1sf1Hj4Jl1", fileId);
      }
      throw new RuntimeException("SQLState = " + e.getSQLState() + " : " + e.getMessage(), e);
    }
  }

  @Override
  public Map<String, String> getAllParams(String fileId) {
    try {
      return getAllFileParams(fileId);
    } catch (SQLException e) {
      final String message = e.getMessage();
      if (message != null && message.startsWith("ORA-00942:")) {
        return new HashMap<>();
      }
      throw new RuntimeException(e);
    }
  }

  @Override
  public String getFileParams(String fileId, String paramName) {
    try {
      return getFileParam(fileId, paramName);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void updateParam(String fileId, Map<String, String> param) {

    try (Connection connection = builder.dataSource.getConnection()) {
      for (Map.Entry<String, String> entry : param.entrySet()) {
        Inserting.InsertInto inserting = Inserting.with(connection)
                                                  .sqlPreparation(this::sql)
                                                  .into("__fileParameterTable__")
                                                  .onConflict("__fileParameterId__")
                                                  .onConflict("__fileParameterName__");
        if (entry.getKey() != null) {
          inserting
            .field("__fileParameterId__", fileId)
            .field("__fileParameterName__", entry.getKey());
          if (entry.getValue() != null) {
            inserting
              .field("__fileParameterValue__", entry.getValue());
          } else {
            deleteExParamByName(fileId, entry.getKey());
            continue;
          }
          inserting.update(entry.getKey(), entry.getValue(), builder.fileParameterValue);
        }
        inserting.go();
      }

    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void updateFileName(String fileId, String newFileName) {
    try (Connection connection = builder.dataSource.getConnection()) {

      try (PreparedStatement ps = connection.prepareStatement(this.sql(
        "update __paramsTable__ set  __paramsTableName__  = ? where __paramsTableId__ = ?"))) {

        ps.setString(1, newFileName);
        ps.setString(2, fileId);
        int updateCount = ps.executeUpdate();

        if (updateCount < 1) {
          throw new NoFileWithId("b34nSHzIgf", fileId);
        }

      }

    } catch (SQLException e) {
      throw new RuntimeException(e.getMessage(), e);
    }

  }

  protected void deleteExParamByName(String fileId, String key) {
    try (Connection connection = builder.dataSource.getConnection()) {

      connection.setAutoCommit(false);
      try {

        doDeleteParam(connection, "delete from __fileParameterTable__ where __fileParameterId__ = ? and __fileParameterName__ = ? ", fileId, key,
                      false);

        connection.commit();

      } catch (SQLException | RuntimeException e) {
        connection.rollback();
        throw e;
      } finally {
        connection.setAutoCommit(true);
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }


  @Override
  public void prepareDatabase(DatabaseNotPrepared context) {
    try {
      prepareDatabaseEx(context);
    } catch (SQLException e) {
      throw prepareException(e);
    }
  }

  private static RuntimeException prepareException(SQLException e) {
    {
      String sqlState = e.getSQLState();
      if ("42P01".equals(sqlState)) {
        throw new DatabaseNotPrepared();
      }
    }
    return new RuntimeException(e);
  }

  private void prepareDatabaseEx(@SuppressWarnings("unused") DatabaseNotPrepared context) throws SQLException {
    int idLen = builder.parent.fileIdLength;
    try (Connection connection = builder.dataSource.getConnection()) {

      try (Query query = new Query(connection)) {
        query.exec(sql("create table __dataTable__ (" +
                         "   __dataTableId__   varchar(40) not null primary key" +
                         ",  __dataTableData__ byteA" +
                         ")"));

        query.exec(sql("create table __paramsTable__ (" +
                         "   __paramsTableId__      varchar(" + idLen + ") not null primary key" +
                         ",  __paramsTableName__    varchar(" + builder.paramsTableNameLength + ")" +
                         ",  __paramsTableMimeType__    varchar(" + builder.paramsTableMimeTypeLength + ")" +
                         ",  __paramsTableDataId__  varchar(40) not null references __dataTable__" +
                         ",  __paramsTableLastModifiedAt__  timestamp not null default current_timestamp" +
                         ")"));

        query.exec(sql("create table __fileParameterTable__ (" +
                         "   __fileParameterId__  varchar(255)" +
                         ",  __fileParameterName__ varchar(255) " +
                         ",  __fileParameterValue__ varchar(255)" +
                         ", PRIMARY KEY (__fileParameterId__, __fileParameterName__) " +
                         ")"));
      }

    }
  }

  @Override
  public FileParams readParams(String fileId) {
    try {
      return readParamsEx(fileId);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  protected FileParams readParamsEx(String fileId) throws SQLException {
    try (Connection connection = builder.dataSource.getConnection()) {

      try (Query query = new Query(connection)) {
        query.sql.append(sql("select * from __paramsTable__ where __paramsTableId__ = ?"));
        query.params.add(fileId);

        query.go();

        if (!query.rs().next()) {
          return null;
        }

        FileParams ret = new FileParams();

        ret.id        = query.rs().getString(builder.getParamsTableId());
        ret.sha1sum   = query.rs().getString(builder.getParamsTableDataId());
        ret.name      = query.rs().getString(builder.getParamsTableName());
        ret.mimeType  = query.rs().getString(builder.getParamsTableMimeType());
        ret.createdAt = query.rs().getTimestamp(builder.getParamsTableLastModifiedAt());

        return ret;
      }

    }

  }

  protected Map<String, String> getAllFileParams(String fileId) throws SQLException {
    try (Connection connection = builder.dataSource.getConnection()) {

      try (Query query = new Query(connection)) {
        query.sql.append(sql(
          "select " + builder.fileParameterName + ", " + builder.fileParameterValue + " from __fileParameterTable__ where __fileParameterId__ = ?"));
        query.params.add(fileId);

        query.go();

        if (!query.rs().next()) {
          return null;
        }

        Map<String, String> ret = new HashMap<>();

        do {
          String name  = query.rs().getString(builder.fileParameterName);
          String value = query.rs().getString(builder.fileParameterValue);
          ret.put(name, value);
        } while (query.rs().next());

        return ret;
      }

    }

  }

  protected String getFileParam(String fileId, String paramName) throws SQLException {
    try (Connection connection = builder.dataSource.getConnection()) {

      try (Query query = new Query(connection)) {
        query.sql.append(
          sql("select " + builder.fileParameterValue + " from __fileParameterTable__ where __fileParameterId__ = ? and __fileParameterName__ = ?"));
        query.params.add(fileId);
        query.params.add(paramName);

        query.go();

        if (!query.rs().next()) {
          return null;
        }

        return query.rs().getString(builder.fileParameterValue);
      }

    }
  }

  @Override
  public byte[] getDataAsArray(String sha1sum) {
    try {
      return getDataAsArrayEx(sha1sum);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  protected byte[] getDataAsArrayEx(String sha1sum) throws SQLException {
    try (Connection connection = builder.dataSource.getConnection()) {

      try (Query query = new Query(connection)) {
        query.sql.append(sql("select __dataTableData__ from __dataTable__ where __dataTableId__ = ?"));
        query.params.add(sha1sum);

        query.go();

        if (!query.rs().next()) {
          throw new RuntimeException("No data for sha1sum = " + sha1sum);
        }
        return query.rs().getBytes(1);
      }

    }
  }
}
