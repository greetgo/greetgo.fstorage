package kz.greetgo.file_storage;

public interface FileUpdatingOperation {

  /**
   * Specifies file name
   *
   * @param name file name
   * @return reference to this
   */
  FileUpdatingOperation name(String name);

  /**
   * Specifies file param
   *
   * @param name name of param
   *
   * @param newValue param Value. If newValue is null then it will delete record
   */
  FileUpdatingOperation param(String name, String newValue);

  /**
   * Run operation
   *
   * @param fileId ID of the file. If file does not exist, throws exception
   */
  void store(String fileId);

}
